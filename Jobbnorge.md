# Integrasjon for arkivering av ansettelsessaker fra Jobbnorge i Documaster

# Beskrivelse av integrasjonen

> Det vil for hver søkeprosess som startes i JobbNorge, opprettes en ny sak automatisk i Documaster. Dette skjer ved førstegangs lagring i JobbNorge. Saken blir da opprettet i Documaster med ekstern identifikator som Jobbnorge genererer.


# Hvor finnes opplysningene?

Med tanke på arkivering av ansettelsessaker tilbyr fra Jobbnorge

- dokumentene som tilhører en ansettelsessak, som PDF-filer samt XML-filer med metadata for registreringene PDF-filene utgjør
- REST-API med metadata for ansettelsessaker (men ikke metadata for registreringer eller med filer)
- en integrasjonsløsning som bruker Documasters API (men som ikke har all funksjonalitet som trengs i UH sak-sammenheng; se nedenfor)

En ansettelsessak kan ha (minst) følgende metadata. Men ikke alle er tilgjengelig via API eller i XML-filen.

Opplysning | REST-API | XML-fil
--- | --- | ---
stillingstittel | `.title` | Kan avledes av (blant annet) `/NOARK.H/saksmappe/tittel`.
nærmeste leder | nei | nei
registrert dato | `.publicationDate` (?) | nei
kundens referanse: | nei | XML-fil: `/NOARK.H/saksmappe/arkivref`
avdeling | `.department` og `.departmentID` | Navnet kan avledes av (blant annet) `/NOARK.H/saksmappe/tittel`.
webside link | nei (ikke samme som `link` i JSON-objektet) | nei
antall stillinger | `.positionCount` | nei
søknadsfrist | `deadline` | nei
utlysningstype (intern/ekstern) | nei? | nei
stillingstype | JSON: `.jobType.id` og `.jobType.name` | Kan avledes av (blant annet) `/NOARK.H/saksmappe/tittel`.
omfang | `.jobScope` | nei
stillingstype varighet (vikariat/midlertidig, fast) | nei | nei
lederstilling | nei | nei
stillingskategori | nei | nei
spesialitet for stillingen | nei | nei
arbeidssted | `.locations[]` | nei

Kort fortalt må man bruke REST-API-et for å hente metadata for selve ansettelsesaken og XML-filene for å hente metadata for registreringene og dokumentene. I tillegg trenger man selve filene.

# Arkivering i Documaster

## Generelle variabler

Tabellen viser hvor en kan hente generelle opplysninger:

Variabel | Type | Kilde | Verdi
--- | --- | --- | ---
`saksbehandlerEpost` | streng | XML-fil fra Jobbnorge | `/NOARK.H/saksmappe/saksansvarlig`
`saksbehandlerNavn` | streng | Brukerobjekt i SCIM | `.Resources[].name.formatted`
`arkivdelId` | heltall | Documaster | `id` for `arkivdel`-objektet med `tittel == "A.d Rekruttering og ansettelse av personale"`
`primaerKlasseId` | heltall | Documaster | `id` for `klasse`-objektet med `refKlassifikasjonssystem.tittel == "FUP"` og `klasseIdent == "A.d.01"`
`sekundaerKlasseIdOU` | heltall | Documaster | `id` for `klasse`-objektet med `refKlassifikasjonssystem.tittel == "OU"` og `klasseIdent %= "%.{admEnhetOuId}"`[^sekundaerKlasseIdOU]

Brukerobjektet fra SCIM får en ved å med et GET-kall `/Users?filter=no:edu:scim:user:userPrincipalName eq "{saksbehandlerEpost}"` (eller lignende[^lignende]). I JSON-objektet SCIM-API-et svarer med, befinner brukerobjektet seg i `'.Resources[]`.

[^lignende]: Eller slik: `'.Resources[] | select(."no:edu:scim:user".userPrincipalName == $saksbehandlerEpost)'`.

[^sekundaerKlasseIdOU]: `klasse.klasseIdent`  Å GJØRE: Forklar `tittel %= "%{admEnhetOuId}"`
`klasse.klasseIdent` for en administrativ enhet består av `ouId`-ene fra øverste enhet ned til enheten det gjelder. For eksempel har enheten «Rektor og styre» `ouId == 761`. For den tilsvarende klassen er `klasseIdent == "1.760.761"`. Siden `ouId` er unik for hver enhet, kan man med uttrykket `klasseIdent %= "%.{admEnhetOuId}"` finne klassen for den administrative enheten. (Les mer om den logiske operatoren `%=` [i Documasters API-dokumentasjon](https://documaster.github.io/api-docs/operations/core/query/).)


### `admEnhetOuId`

Dette er `ouId`-en i Orgreg for den administrative enheten hvor dokumentasjonen skal arkiveres. Hvilken verdi som brukes, er avhengig av dokumentasjonen skal arkiveres ved den administrative enheten saksbehandleren eller den utlyste stillingen tilhører:

#### Arkivering ved den administrative enheten som saksbehandleren tilhører

Hvis en tar utgangspunkt i `/NOARK.H/saksmappe/saksansvarlig` fra XML-filer i Documaster, vil dokumentasjonen bli arkivert ved den administrative enheten vedkommende tilhører. En finner da `ouId`-en som følger:

Variabel | Type | Kilde | Verdi
--- | --- | --- | ---
`admEnhetAkronym` | streng | SCIM | `.Resources[]."no:edu:scim:user".primaryOrgUnit.symbol`
`admEnhetOuId` | heltall | Orgreg | `.[] \| select(.acronym.nob == {admEnhetAkronym}) \| .ouId`


#### Arkivering ved den administrative enheten som den utlyste stillingen tilhører

Hvis dokumentasjonen skal arkiveres ved den administrative enheten som stillingen tilhører, må en ta utgangspunkti. `departmentID` fra Jobbnorges REST-API.

En kan trolig hente ut en liste over administrative enheter i Jobbnorge ved å bruke et av `/department/*`-endepunktene i Jobbnorge Integration API. Disse kan en da legge til som en `externalKey` i Orgreg. Da får en `admEnhetOuId` som følgende:

Variabel | Type | Kilde | Verdi
--- | --- | --- | ---
`departmentID` | heltall | Jobbnorge REST-API | `.departmentID`
`admEnhetOuId` | heltall | Orgreg | `.[] \| select(.externalKeys[].type == "departmentID" and .externalKeys[].sourceSystem == "jobbnorge" and .externalKeys[].value == "{departmentID}") \| .ouId`[^forhold]

[^forhold]: Forutsetter at ikke flere administrative enheter i Orgreg har samme `departmentID`-attributt.


## Mappe

Opprett en saksmappe og knytt den til

* arkivdel
* primær klasse i klassifikasjonssystemet «FUP»
* sekundær klasse i klassifikasjonssystemet «OU»

Saksmappen skal også knyttes til en klasse i klassifikasjonssystemet «ROLF». Men dette skal mest sannsynlig ikke gjøres av integrasjonen.

**Å GJØRE:** Beskriv avslutning av saksmappen.

`save`:

Felt | Type | Kilde | Verdi
--- | --- | --- | ---
`type` | streng | | `"Saksmappe"`
`fields.tittel` | streng | Jobbnorge REST-API | `.stillingstittel`
`fields.administrativEnhet` | streng | generelle variabler | `admEnhetOuId`
`fields.saksansvarlig` | streng | generelle variabler | `saksbehandlerNavn`
`fields.saksansvarligBrukerIdent` | streng | generelle variabler | `saksbehandlerEpost`
`fields.saksstatus` | streng | | `"B"` |

`link`:

Felt | Type | Kilde | Verdi
--- | --- | --- | ---
`refArkivdel` | heltall | Documaster | `arkivdelId`
`refPrimaerKlasse` | heltall | Documaster | `primaerKlasseId`
`refSekundaerKlasse` | heltall | Documaster | `sekundaerKlasseIdOU`
`refEksternId` | heltall | Documaster | `eksternId.id`


## EksternId

Opprett en eksternId og knytt den til saksmappen.

`save`:

Felt | Type | Kilde | Verdi
--- | --- | --- | ---
`type` | streng | | `"EksternId"` |
`fields.eksterntSystem` | streng | | `"Jobbnorge"`
`fields.eksternID` | streng | XML-fil fra Jobbnorge | `/NOARK.H/saksmappe/stillingsid`

`link`:

Felt | Type | Kilde | Verdi
--- | --- | --- | ---
`refMappe` | heltall | Documaster | `id`-en til saksmappen


# Sekundære klasser utenom OU og ROLF

For eksempel stillingskode. Kommer.

<!--
`save`:

Felt | Kilde | Verdi
--- | --- | --- | ---
`type` | | `"Klasse"` |
`fields.klasseIdent` | streng | |
`fields.tittel` | streng | |

`link`:

Felt | Type | Kilde | Verdi
--- | --- | --- | ---
`refMappe` | heltall | Documaster | `id`-en til saksmappen
-->


## Registrering

For hver XML-fil fra Jobbnorge opprett en registrering:

**Å GJØRE:** Avklar verdi for `korrespondansepartNavn`?

**Å GJØRE:** `skjermKorrespondanseParterEInnsyn` har alltid verdi `false`?

**Å GJØRE:** `skjerming`-verdien kan trolig variere fra institusjon til institusjon. Verdiene nedenfor er fra AT-miljøet.

**Å GJØRE:** Hvilke metadata kan brukes for å angi om dokumentet kan fulltekstpubliseres på eInnsyn? (Må kunne filtreres i Documaster.)

**Å GJØRE:** Legg til [avskriving](https://documaster.github.io/api-docs/model/entities/abstractrecord/registryentry/#sign-off).

**Å GJØRE:** Hva gjør en i tilfeller hvor færre en normalt skal ha tilgang til skjermede dokumenter, for eksempel fordi en av søkerne tilhører samme enhet som dokumentene arkiveres hos? Vil leder ved enheten dokumentene arkiveres hos, alltid ha tilgang? Da må de arkiveres et annet sted. Hvis ikke er det kanskje mulig å begrense tilgang til bare saksbehandler. Men dette kan per i dag bare styres fra ServiceNow.

`save`:

Felt | Type | Kilde | Verdi
--- | --- | --- | ---
`type` | streng | | `"Journalpost"`
`fields.tittel` | streng | XML-fil fra Jobbnorge | `/NOARK.H/saksmappe/journalpost/tittel`
`fields.journalansvarlig` | streng | generelle variabler | `saksbehandlerNavn`
`fields.journalansvarligBrukerIdent` | streng | generelle variabler | `saksbehandlerEpost`
`fields.journalposttype` | streng | XML-fil fra Jobbnorge | `/NOARK.H/saksmappe/journalpost/journalposttype`
`fields.dokumentetsDato` | dato | XML-fil fra Jobbnorge | `/NOARK.H/saksmappe/journalpost/dokumentetsDato`
`fields.skjerming` | streng | Se nedenfor. | Se nedenfor. |

Tittel | `skjerming` | Fulltekstpublisering
--- | --- | ---
Jobbanalyse | ? | ?
Stillingsannonse | nei | ja
Søknad/CV | nei | nei
Offentlig søkerliste | nei | ja
Utvidet søkerliste | `"offl25"` | nei
Postmeldinger dvs. intern kommunikasjon med jobbsøkere i Jobbadmin | `"offl25"` | nei
Innstillingsrapport ( + andre lignende dokumenter som opprettes på stillingsnivå i Jobbadmin) | `"offl131leddjffvl131leddnr1"` | nei
Kandidatdokumenter (arbeidsavtaler, instrukser og lignende, evt. Med digital signatur) | ? | nei
Journaldokument (uttrekk av journalpliktige hendelser fra logg i Jobbadmin) | ? | ?


`link`:

Felt | Type | Kilde | Verdi
--- | --- | --- | ---
`refMappe` | heltall | Documaster | `id`-en til saksmappen


## Korrespondanseparter

Opprett korrespondanseparter og knytt dem til registreringen:

`save`:

Felt | Type | Kilde | Verdi
--- | --- | --- | ---
`type` | streng | | `"Korrespondansepart"`
`fields.korrespondanseparttype` | streng | Se nedenfor. | Se nedenfor.
`fields.korrespondansepartNavn` | streng | Se nedenfor. | Se nedenfor.

Person | `korrespondansepartNavn`
--- | ---
saksbehandler | generelle variabler: `saksbehandlernavn`
søker | XML-fil fra Jobbnorge: `/NOARK.H/saksmappe/journalpost/korrespondansepart/korrespondansepartNavn`

`journalposttype` | `korrespondanseparttype`
--- | ---
`"I"` | søker som `"EA"` og saksbehandler som `"IM"`
`"U"` | søker som `"EM"` og saksbehandler som `"IA"`
`"X"` | saksbehandler som `"IA"`

`link`:

Felt | Type | Kilde | Verdi
--- | --- | --- | ---
`refRegistrering` | heltall | Documaster | `id`-en til registreringen


<!--
fields.virksomhetsspesifikkeMetadata kunne blitt brukt for kandidatID
-->

<!--
Dokumenttypen Søknad/CV kan filtreres på kandidatestatus, dvs en kan velge mellom å
få eksportert alle innkomne søknader, søknader kun for den/de som ansettes eller
søknad for den/de som ansettes og de som er innstilt.
Dokumenttypen Postmeldinger kan filtreres på kategorier. Kunden kan velge hvilke
postmeldinger som ønskes arkivert basert på kategorier:

Avslag på søknad
Forespørsel om oppdatering av søknad
Tilbud om ansettelse
Søkerliste
Melding tilknyttet utesending av kandidatdokument
Innkalling til intervju (intervjuslots)
Avslag unntatt offentlighet
Søknad trukket (bekreftelse fra saksbehandler)
Unntatt offentlighet (bekreftelse fra saksbehandler)
-->


## Dokumenter

For hvert dokument (`dokumentNr`) angitt i XML-filen fra Jobbnorge last opp dokumentfilen, opprett dokument og dokumentversjon, knytt dem sammen og knytt dokumentet til registreringen:

**Å GJØRE:** Beskriv dokumentversjon

`save`:

Felt | Type | Kilde | Verdi
--- | --- | --- | ---
`type` | streng | | `"Dokument"`
`fields.tittel` | streng | XML-fil fra Jobbnorge | `/NOARK.H/saksmappe/journalpost/dokumenter[{dokumentNr}]/tittel`
`fields.tilknyttetRegistreringSom` | streng | XML-fil fra Jobbnorge | `/NOARK.H/saksmappe/journalpost/dokumenter[{dokumentNr}]/tilknyttetRegistreringSom`

`link`:

Felt | Type | Kilde | Verdi | Eksempel
--- | --- | --- | --- | ---
`refRegistrering` | heltall | Documaster | `id`-en til registreringen |


<!--

**Metadata sak **

+-------------------------------------------------------------------------------------------------+--------------------------------------+----------------+--------------------------+----------------+---------------------------------------------+
| Dokumenter som arkiveres. (Legg til/fjern linjer med dokument-typer kunde har valgt å arkivere) | Dokumenttyper (JP.DOKTYPE)           | Status\        | Skjerming lovhenvisning  | Klassifikasjon | Einnsyn (fulltekstpublisering Ja eller Nei) |
|                                                                                                 |                                      | (JP.STATUS)    |                          |                |                                             |
|                                                                                                 | U : Utgående                         |                |                          |                |                                             |
|                                                                                                 |                                      | J : Jornalført |                          |                |                                             |
|                                                                                                 | I : Innkommende                      |                |                          |                |                                             |
|                                                                                                 |                                      | E : Ekspedert  |                          |                |                                             |
+=================================================================================================+======================================+================+==========================+================+=============================================+
| Annonser                                                                                        | Internt notat/e-post uten oppfølging | J              |                          | A.d.01         | Ja                                          |
+-------------------------------------------------------------------------------------------------+--------------------------------------+----------------+--------------------------+----------------+---------------------------------------------+
| Søknad og CV                                                                                    | Dokument inn                         | J              | offl. § 25               | A.d.01         | Nei                                         |
|                                                                                                 |                                      |                |                          |                |                                             |
| Til den som ansettes. Overføres manuelt?                                                        |                                      |                |                          |                |                                             |
+-------------------------------------------------------------------------------------------------+--------------------------------------+----------------+--------------------------+----------------+---------------------------------------------+
| Utvidet\                                                                                        | Internt notat/e-post uten oppfølging | J              | offl. § 25               | A.d.01         | Nei                                         |
| søkerliste                                                                                      |                                      |                |                          |                |                                             |
+-------------------------------------------------------------------------------------------------+--------------------------------------+----------------+--------------------------+----------------+---------------------------------------------+
| Offentlig\                                                                                      | Internt notat/e-post uten oppfølging | J              |                          | A.d.01         | Ja                                          |
| søkerliste                                                                                      |                                      |                |                          |                |                                             |
+-------------------------------------------------------------------------------------------------+--------------------------------------+----------------+--------------------------+----------------+---------------------------------------------+
| Innstillingsrapport                                                                             | Internt notat/e-post uten oppfølging | E              | offl. § 13 1.ledd        | A.d.01         | Nei                                         |
|                                                                                                 |                                      |                |                          |                |                                             |
|                                                                                                 |                                      |                | fvl. § 13 1.ledd punkt 1 |                |                                             |
+-------------------------------------------------------------------------------------------------+--------------------------------------+----------------+--------------------------+----------------+---------------------------------------------+
| Journaldokument                                                                                 | Internt notat/e-post uten oppfølging | J              | offl. § 25               | A.d.01         | Nei                                         |
+-------------------------------------------------------------------------------------------------+--------------------------------------+----------------+--------------------------+----------------+---------------------------------------------+

: Tabel 1 Dokument-type oversikt

> Postmeldinger skal de utelates -- er det mulig å overføre enkelte meldinger manuelt for saksbehandler fra JobbNorge? Postmelding -- manuell overføring, legg til i tabell.

-   Omfatter også trekk av søknad.

-   **Eksternskjerming:\
    **Ved automatisk opprettelse, vil saken selv vil være offentlig. Journalpostene (dokumentene i DM) vil følge tabellen ovenfor (bestemt av tilgangskoden).

-   **Internskjerming:\
    **Ved automatisk opprettelse, vil saken selv vil være offentlig. Journalpostene vil eventuelt bli skjermet etter Rolf/FUP.

-   **Automatisk journalføring:\
    **Annonse, søknad m/CV og vitnemål + attester settes til status J -- Journalført, og inngående dokumenter avskrives direkte. Alle avsender/mottaker skjermes på dokumentene som er unntatt offentlighet.

-   **Mottaker/avsender :\
    **Alle dokumenter av type "Dokument inn" eller "Dokument ut", vil få satt Avsender og Mottaker som finnes i JobbNorge. Avsender/Mottaker på institusjonsside vil være satt til saksbehandler, mens mot parten vil som oftest være søkeren.

**Metadata journalpost Rekruttering**

  --------------------------------------------------------------------------------------------
  *Metadata *                     eksemple                    *Kilde*        *Forutsetning* 
  ------------------------------ ---------------------------- -------------- -----------------
  *Tittel *                      Hentes fra JobbNorge         JobbNorge      

  *offentligTittel*              Lik tittel                                  

  *Opprettet dato*               Opprettet dato i JobbNorge   JobbNorge      

  *Type*                         (I, U, N, X)                                Etter tabell 1

  *Status*                       Journalført                                 Etter tabell 1

  *Registreringsdato*            Lik opprettet dato           JobbNorge      

  *Dokumentdato*                 Lik opprettet dato           JobbNorge      

  *Sendt dato*                   Lik opprettet dato           JobbNorge      

  *Mottatt dato*                                              JobbNorge      

  *journalansvarlig*             saksansvarlig                JobbNorge      

  *Skjerming*                                                                Etter tabell 1

  *Publiseringsstatus*                                                       

  *eInnsyn*                                                                  Etter tabell 1

  *Korrespondansepart*                                        JobbNorge      

  *Dokumentstatus (Filstatus)*   F - Ferdig                                  
  --------------------------------------------------------------------------------------------

**\
**

**Saksbehandling i ServiceNow**

Det må opprettes prosesser i ServiceNow for de prosessene som krever saksbehandling. Eksempel:

> **Utsendelse av tilbudsbrev**
>
> Lage mal i servicenow for tilbudsbrev, og vedlegg. Som så sendes til mottaker fra servicenow. Tilbudsbrevet skal signeres med eSignering. FUP -- A.d.02

Tabel 2 Journalpost-oversikt -- saksbehandling i ServiceNow

+--------------------------------------------------------------+--------------------------------------+----------------+--------------------------------+--------------------------+----------------+---------------------------------------------+
| Dokumenter som arkiveres.                                    | Dokumenttyper (JP.DOKTYPE)           | Status\        | Skjerming\                     | Skjerming 2              | Klassifikasjon | Einnsyn (fulltekstpublisering Ja eller Nei) |
|                                                              |                                      | (JP.STATUS)    | (JP.U1)                        |                          |                |                                             |
|                                                              | U : Utgående                         |                |                                | Paragraf(er)             |                |                                             |
|                                                              |                                      | J : Jornalført | U : offentlig (ugradert)       |                          |                |                                             |
|                                                              | I : Innkommende                      |                |                                |                          |                |                                             |
|                                                              |                                      | E : Ekspedert  | UO: unntatt-offentlig          |                          |                |                                             |
+==============================================================+======================================+================+================================+==========================+================+=============================================+
| Behovskartlegging/ressursplanlegging                         | Internt notat/e-post uten oppfølging | J              | UO                             | offl. § 25               | A.d.01         | Nei                                         |
+--------------------------------------------------------------+--------------------------------------+----------------+--------------------------------+--------------------------+----------------+---------------------------------------------+
|                                                              |                                      |                |                                |                          |                |                                             |
+--------------------------------------------------------------+--------------------------------------+----------------+--------------------------------+--------------------------+----------------+---------------------------------------------+
| Tilbud om ansettelse                                         | Dokument ut                          | J              | UO                             | offl. § 25               | A.d.02         | Nei                                         |
+--------------------------------------------------------------+--------------------------------------+----------------+--------------------------------+--------------------------+----------------+---------------------------------------------+
| Forhandling av lønn/vilkår                                   | Dokument inn                         | J              | UO                             | fvl. § 13 1.ledd punkt 2 | A.d.02         | Nei                                         |
|                                                              |                                      |                |                                |                          |                |                                             |
|                                                              | Dokument ut                          |                |                                |                          |                |                                             |
+--------------------------------------------------------------+--------------------------------------+----------------+--------------------------------+--------------------------+----------------+---------------------------------------------+
| Avslag                                                       | Dokument inn                         | J              | UO                             | offl. § 13 1.ledd        | A.d.02         | Nei                                         |
|                                                              |                                      |                |                                |                          |                |                                             |
|                                                              |                                      |                |                                | fvl. § 13 1.ledd punkt 1 |                |                                             |
+--------------------------------------------------------------+--------------------------------------+----------------+--------------------------------+--------------------------+----------------+---------------------------------------------+
| Arbeidsavtale (via eSignering eller oversendt pr post/epost) | Dokument inn                         | J              | UO Avsender skal ikke skjermes | offl. § 26 5.ledd        | A.d.02         | Nei                                         |
+--------------------------------------------------------------+--------------------------------------+----------------+--------------------------------+--------------------------+----------------+---------------------------------------------+
| Innhente personalopplysninger                                | Dokument inn                         | J              |                                | fvl. § 13 1.ledd punkt 1 | A.d.02         | Nei                                         |
+--------------------------------------------------------------+--------------------------------------+----------------+--------------------------------+--------------------------+----------------+---------------------------------------------+

: Tabel 1 Dokument-type oversikt

Arbeidsavtale vil den inneholde fødsels og personnr eller kun fødselsdato?

Vil det være mulig for mottaker å oppgi personalopplysninger ved å logge inn i SN?

**\
**

**Metadata Journalpost -- Prosessnavn**

  -----------------------------------------------------------------------------------------------------------------------------------------------------------
  *Metadata*                    eksemple                                                  *Kilde*        *Forutsetning* 
  ---------------------------- ---------------------------------------------------------- -------------- ----------------------------------------------------
  Tittel                       Prosess                                                    ServiceNow     

  offentligTittel              Lik tittel                                                                

  Arkiv/Arkivdel               A.d                                                        Sak            

  PrimærKlasse                 A.d.02                                                     Sak            

  SekundærKlasse                                                                                         

  Opprettet dato               Opprettet dato i ServiceNow                                ServiceNow     

  Beskrivelse                                                                                            

  Type                         I/U/X                                                                     Etter tabell 2

  Status                       Journalført                                                               Etter tabell 2

  Registreringsdato            Dato for opprettet prosess                                 ServiceNow     

  Dokumentdato                 Dato for avslutning av prosess / Ekspedert                 ServiceNow     

  Sendt dato                   Dato for avslutning av prosess / Ekspedert                 ServiceNow     

  Mottatt dato                 Dato for mottak i ServiceNow                               ServiceNow     

  journalansvarlig             saksansvarlig                                              ServiceNow     

  Avsluttet dato                                                                                         

  Forfallsdato                                                                                           

  Skjerming                                                                                              Etter tabell 2

  Nøkkelord                                                                                              

  Publiseringsstatus                                                                                     Etter tabell 2

  eInnsyn                                                                                                

  Korrespondansepart           Mottaker/avsender/kopi angitt i SN                         ServiceNow     

  Dokumentstatus (Filstatus)   F - Ferdig                                                                

                                                                                                         

  *Saksnr og dok.nr*           Opprettes ved overføring til DM? Eller overføres til SN?   Documaster     Ved overføring til DM vil det angis sak. og dok.nr
  -----------------------------------------------------------------------------------------------------------------------------------------------------------

**\
**

-   **Andre DM Arkivspesifikke avklaringer:**

+------------------------------------------------------+----------------------------------------------------+
| > **Sak**                                            | > **Løsning**                                      |
+======================================================+====================================================+
| > Hvilken sakstype skal opprettes?                   | > Sak                                              |
+------------------------------------------------------+----------------------------------------------------+
| > Hvilken saksstatus skal settes?                    | > «Under behandling»                               |
+------------------------------------------------------+----------------------------------------------------+
| > Hvilket delarkiv skal saken ligge i?               | > A - Internadministrasjon                         |
+------------------------------------------------------+----------------------------------------------------+
| > Dokument                                           | > Løsning                                          |
+------------------------------------------------------+----------------------------------------------------+
| > Hvilket dokumentarkiv skal dokumentene tilknyttes? | > A.d                                              |
+------------------------------------------------------+----------------------------------------------------+
| > Hvilken paragraf skal dokumentene tilknyttes?      | > Se tabell 2 ovenfor                              |
+------------------------------------------------------+----------------------------------------------------+

-->

# Integrasjonsmuligheter ved arkivering fra Jobbnorge

## PDF-filer og XML-filer med metadata

> XML filene er bygd med samme prinsipp uansett hvilket sak/arkiv system som er mottaker. Vi har integrasjon mot ePhorte, Elements, Acos Websak og P360. Det er i utgangspunktet en standard XML pr system, men den blir ofte tilpasset etter behov fordi kravet om metadata varier fra kunde til kunde

> [D]et er mulig å utvide XML [med felt som som avdeling, utlysningstype, stillingstype, omfang og varighet] men alle slike tilpasninger av XML må estimeres og utvikles.

Se eksempler på XML-filer nedenfor.

## Jobbnorge Public API

Kan brukes til å «hente ut stillinger som er publisert på jobbnorge og for å liste disse opp på egne hjemmesider».

Det ser ut som det ikke finnes noe endepunkt for å spørre etter en stilling med en bestemt ID, men man kan bruke ID-en som søketerm, for eksempel slik: <https://publicapi.jobbnorge.no/v2/Jobs?term=253973>. (For sikkerhets skyld bør en eksplisitt velge stillingen med denne id-en i responsen.)

Hvis en filtrerer på arbeidsgiver på jobbnorge.no, kan kan en se arbeidsgiverens id i nettadressen. Slik kan en spørre etter utlysningene for arbeidsgiveren med id 724 (UiB): https://publicapi.jobbnorge.no/v1/Jobs?employer=724

Eksempel:

```json
[
  {
    "id": 253973,
    "jobScope": "Heltid",
    "jobDuration": "14.12.2023",
    "deadline": "14.12.2023",
    "employer": "Universitetet i Bergen",
    "employerID": 724,
    "regardDepartmentAsEmployer": false,
    "department": "Økonomiavdelingen",
    "departmentID": 505,
    "title": "Saksbehandler økonomi",
    "summary": "Gjennom eit sterkt og tett samspel med omverda - globalt, nasjonalt og lokalt - skal vi medverke til eit samfunn bygd på kunnskap,",
    "logo": "https://www.jobbnorge.no/logos/724/37fff836-eecc-4dcd-a523-494f43e7037b.png",
    "link": "https://www.jobbnorge.no/ledige-stillinger/stilling/253973",
    "promoted": false,
    "positionCount": 1,
    "locations": [
      {
        "address": "Nygårsgaten 5",
        "area": "Bergen",
        "municipality": "Bergen",
        "county": "Vestland",
        "isDomestic": true,
        "isPrimary": true,
        "zipCode": "5015"
      }
    ],
    "publicationDate": "22.11.2023",
    "jobType": {
      "id": 926,
      "name": "1363 Seniorkonsulent"
    }
  }
]
```

Dokumentasjon: <https://publicapi.jobbnorge.no/swagger/index.html>


## Jobbnorge Integration API

> Gjennom API kan man hente ut data ut fra behov (f.eks.om stillingen, kandidatens personalia og CV-opplysninger). Tilgang til Jobbnorge Integration API er håndtert gjennom tokens som opprettes av kunde selv i rekrutteringsverktøyet.

Men: API-et «kan [bare] returnere samme data som vi har i Public API».

En må antakeligvis bruke dette API-et for å hente stillinger som ikke lenger er publisert.

Dokumentasjon: <https://api.jobbnorge.no/swagger/index.html>


## Integrasjon fra Jobbnorge for Documaster

> Når det gjelder P360 og Documaster har vi laget en ny type integrasjon som baserer seg på bruk av REST-API, henholdsvis SIF API eller Documaster API.

Men vi kan neppe bruke denne integrasjonsløsningen uten tilpasninger. Den har ikke funksjonalitet for å bruke OU-klassifikasjonsystemet.

Det er usikkert om integrasjonen kan autentisere seg mot Documaster via Gravitee.


# XML-filer mottatt fra Jobbnorge

## `20220407132243_123456_335178_2869005_PublicApplicantsList.xml`

```xml
<?xml version="1.0" encoding="utf-8"?>
<NOARK.H xmlns:jn="http://export.jobbnorge.no/xml/" xmlns:extensions="Jobbnorge.Export.Helpers" version="1.0">
  <saksmappe>
    <stillingsid>123456</stillingsid>
    <arkivref>2022/123</arkivref>
    <tittel>Ledig stilling som Rådgiver i Økonomiavdelingen i Rådgivertjenester AS - 2022/123</tittel>
    <saksansvarlig>ola.normann.test.com</saksansvarlig>
    <journalpost>
      <tittel>Offentlig søkerliste for stilling - Rådgiver i Økonomiavdelingen i Rådgivertjenester AS - 2022/123</tittel>
      <dokumentetsDato>20220407</dokumentetsDato>
      <untattoffentlighet>false</untattoffentlighet>
      <journalposttype>X</journalposttype>
      <korrespondansepart>
        <kandidatID/>
        <korrespondansepartNavn/>
        <postadresse/>
        <postnummer/>
        <poststed/>
        <epostadresse/>
        <telefonnummer/>
      </korrespondansepart>
      <dokumenter>
        <dokumentbeskrivelse>
          <tittel>Offentlig søkerliste for stilling - Rådgiver i Økonomiavdelingen i Rådgivertjenester AS - 2022/123</tittel>
          <format>PDF</format>
          <referanseDokumentfil>20220407132241_123456_335178_2869004_PublicApplicantsList.pdf</referanseDokumentfil>
          <tilknyttetRegistreringSom>H</tilknyttetRegistreringSom>
        </dokumentbeskrivelse>
      </dokumenter>
    </journalpost>
  </saksmappe>
</NOARK.H>
```

## `20220407132246_123456_335179_2869007_ExtendedApplicantsList.xml`

```xml
<?xml version="1.0" encoding="utf-8"?>
<NOARK.H xmlns:jn="http://export.jobbnorge.no/xml/" xmlns:extensions="Jobbnorge.Export.Helpers" version="1.0">
  <saksmappe>
    <stillingsid>123456</stillingsid>
    <arkivref>2022/123</arkivref>
    <tittel>Ledig stilling som Rådgiver i Økonomiavdelingen i Rådgivertjenester AS - 2022/123</tittel>
    <saksansvarlig>ola.normann.test.com</saksansvarlig>
    <journalpost>
      <tittel>Utvidet søkerliste for stilling - Rådgiver i Økonomiavdelingen i Rådgivertjenester AS - 2022/123</tittel>
      <dokumentetsDato>20220407</dokumentetsDato>
      <untattoffentlighet>true</untattoffentlighet>
      <journalposttype>X</journalposttype>
      <korrespondansepart>
        <kandidatID/>
        <korrespondansepartNavn/>
        <postadresse/>
        <postnummer/>
        <poststed/>
        <epostadresse/>
        <telefonnummer/>
      </korrespondansepart>
      <dokumenter>
        <dokumentbeskrivelse>
          <tittel>Utvidet søkerliste for stilling - Rådgiver i Økonomiavdelingen i Rådgivertjenester AS - 2022/123</tittel>
          <format>PDF</format>
          <referanseDokumentfil>20220407132245_123456_335179_2869006_ExtendedApplicantsList.pdf</referanseDokumentfil>
          <tilknyttetRegistreringSom>H</tilknyttetRegistreringSom>
        </dokumentbeskrivelse>
      </dokumenter>
    </journalpost>
  </saksmappe>
</NOARK.H>
```

## `20220407132250_123456_335180_2869009_JobText.xml`

```xml
<?xml version="1.0" encoding="utf-8"?>
<NOARK.H xmlns:jn="http://export.jobbnorge.no/xml/" xmlns:extensions="Jobbnorge.Export.Helpers" version="1.0">
  <saksmappe>
    <stillingsid>123456</stillingsid>
    <arkivref>2022/123</arkivref>
    <tittel>Ledig stilling som Rådgiver i Økonomiavdelingen i Rådgivertjenester AS - 2022/123</tittel>
    <saksansvarlig>ola.normann.test.com</saksansvarlig>
    <journalpost>
      <tittel>Kunngjøring av ledig stilling - Rådgiver - i Økonomiavdelingen ved Rådgivertjenester AS - 2022/123</tittel>
      <dokumentetsDato>20210304</dokumentetsDato>
      <untattoffentlighet>false</untattoffentlighet>
      <journalposttype>X</journalposttype>
      <korrespondansepart>
        <kandidatID/>
        <korrespondansepartNavn/>
        <postadresse/>
        <postnummer/>
        <poststed/>
        <epostadresse/>
        <telefonnummer/>
      </korrespondansepart>
      <dokumenter>
        <dokumentbeskrivelse>
          <tittel>Kunngjøring av ledig stilling - Rådgiver i Økonomiavdelingen ved Rådgivertjenester AS - 2022/123</tittel>
          <format>PDF</format>
          <referanseDokumentfil>20220407132248_123456_335180_2869008_JobText.pdf</referanseDokumentfil>
          <tilknyttetRegistreringSom>H</tilknyttetRegistreringSom>
        </dokumentbeskrivelse>
      </dokumenter>
    </journalpost>
  </saksmappe>
</NOARK.H>
```

## `20220407132252_123456_335181_2869011_MailDialog.xml`

```xml
<?xml version="1.0" encoding="utf-8"?>
<NOARK.H xmlns:jn="http://export.jobbnorge.no/xml/" xmlns:extensions="Jobbnorge.Export.Helpers" version="1.0">
  <saksmappe>
    <stillingsid>123456</stillingsid>
    <arkivref>2022/123</arkivref>
    <tittel>Ledig stilling som Rådgiver - i Økonomiavdelingen i Rådgivertjenester AS - 2022/123</tittel>
    <saksansvarlig>ola.normann.test.com</saksansvarlig>
    <journalpost>
      <tittel>Avslag på søknad for @Kari Normann</tittel>
      <dokumentetsDato>20210628</dokumentetsDato>
      <untattoffentlighet>true</untattoffentlighet>
      <journalposttype>U</journalposttype>
      <korrespondansepart>
        <kandidatID>123123</kandidatID>
        <korrespondansepartNavn>Kari Normann</korrespondansepartNavn>
        <postadresse>Parkveien 1</postadresse>
        <postnummer>0254</postnummer>
        <poststed>Oslo</poststed>
        <epostadresse>kari@test.com</epostadresse>
        <telefonnummer>11111111</telefonnummer>
      </korrespondansepart>
      <dokumenter>
        <dokumentbeskrivelse>
          <tittel>Avslag på søknad - @Kari Normann</tittel>
          <format>PDF</format>
          <referanseDokumentfil>20220407132252_123456_335181_2869010_MailDialog.pdf</referanseDokumentfil>
          <tilknyttetRegistreringSom>H</tilknyttetRegistreringSom>
        </dokumentbeskrivelse>
      </dokumenter>
    </journalpost>
  </saksmappe>
</NOARK.H>
```

## `20220407132311_123456_335182_2869045_Application.xml`

```xml
<?xml version="1.0" encoding="utf-8"?>
<NOARK.H xmlns:jn="http://export.jobbnorge.no/xml/" xmlns:extensions="Jobbnorge.Export.Helpers" version="1.0">
  <saksmappe>
    <stillingsid>123456</stillingsid>
    <arkivref>2022/123</arkivref>
    <tittel>Ledig stilling som Rådgiver i Økonomiavdelingen i Rådgivertjenester AS - 2022/123</tittel>
    <saksansvarlig>ola.normann.test.com</saksansvarlig>
    <journalpost>
      <tittel>Søknad på stilling - Rådgiver i Økonomiavdelingen i Rådgivertjenester AS - 2022/123 - @Anna Hansen</tittel>
      <dokumentetsDato>20211124</dokumentetsDato>
      <untattoffentlighet>true</untattoffentlighet>
      <journalposttype>I</journalposttype>
      <korrespondansepart>
        <kandidatID>222222</kandidatID>
        <korrespondansepartNavn>Anna Normann</korrespondansepartNavn>
        <postadresse>Parkveien 3</postadresse>
        <postnummer>0350</postnummer>
        <poststed>Oslo</poststed>
        <epostadresse>anna@test.com</epostadresse>
        <telefonnummer>
        </telefonnummer>
      </korrespondansepart>
      <dokumenter>
        <dokumentbeskrivelse>
          <tittel>Søknad på stilling - Rådgiver i Økonomiavdelingen i Rådgivertjenester AS - 2022/123 - @Anna Hansen</tittel>
          <format>PDF</format>
          <referanseDokumentfil>20220407132310_123456_335182_2869044_Application.pdf</referanseDokumentfil>
          <tilknyttetRegistreringSom>H</tilknyttetRegistreringSom>
        </dokumentbeskrivelse>
      </dokumenter>
    </journalpost>
  </saksmappe>
</NOARK.H>
```

# XML-filer mottatt fra Sikt (konvertert til JSON)

```json
{
  "NOARK_H": {
    "NOARKSAK": {
      "STILLINGSID": "251061",
      "SA_TITTEL": "PhD position within The Use of Wood in Future Sustainable Hospitals - 23/03854",
      "SA_TITTELINTERN": null,
      "SA_ANSVFIRMANAVN": "Norges miljø- og biovitenskapelige universitet (NMBU)",
      "SA_ARKIVREF": "23/03854",
      "SA_ANSVEPOST": "kari.nordmann@nmbu.no",
      "JOURNALPOST_TAB": {
        "JOURNALPOST": {
          "JP_INNHOLD": "Postmelding - RE: Intervju PhD-stilling NMBU - 23/03854",
          "JP_U1": "UO",
          "JP_DOKDATO": "29.11.2023",
          "JP_DOKTYPE": "Dokument inn",
          "ADRESSAT_TAB": {
            "ADRESSAT": {
              "NAVN": "Hanne xxx",
              "ADRESSE": null,
              "POSTNR": null,
              "POSTSTED": null,
              "EPOSTADR": "xxxx.xxx@gmail.com",
              "TLF": null
            }
          },
          "DOKVERSJON_TAB": {
            "DOKVERSJON": [
              {
                "VE_NAVN": "Postmelding",
                "VE_DOKFORMAT": "PDF",
                "VE_FILREF": "20231129100141_251061_431274_3983111_CollectionMailDialog.pdf",
                "VE_DOKID": null
              }
            ]
          }
        }
      }
    }
  }
}
{
  "NOARK_H": {
    "NOARKSAK": {
      "STILLINGSID": "251323",
      "SA_TITTEL": "Instituttleder ved Institutt for prosess-, energi- og miljøteknologi",
      "SA_TITTELINTERN": null,
      "SA_ANSVFIRMANAVN": "Universitetet i Sørøst-Norge",
      "SA_ARKIVREF": "23/19056",
      "SA_ANSVEPOST": "kari.nordmann@usn.no",
      "JOURNALPOST_TAB": {
        "JOURNALPOST": {
          "JP_INNHOLD": "Journaldokument  - 23/117",
          "JP_U1": "UO",
          "JP_DOKDATO": "29.11.2023",
          "JP_DOKTYPE": "Internt notat/e-post uten oppfølging",
          "ADRESSAT_TAB": {
            "ADRESSAT": {
              "NAVN": null,
              "ADRESSE": null,
              "POSTNR": null,
              "POSTSTED": null,
              "EPOSTADR": null,
              "TLF": null
            }
          },
          "DOKVERSJON_TAB": {
            "DOKVERSJON": [
              {
                "VE_NAVN": "Journaldokument",
                "VE_DOKFORMAT": "PDF",
                "VE_FILREF": "20231129100117_251323_431273_3983109_JournalDoc.pdf",
                "VE_DOKID": null
              }
            ]
          }
        }
      }
    }
  }
}
{
  "NOARK_H": {
    "NOARKSAK": {
      "STILLINGSID": "251323",
      "SA_TITTEL": "Instituttleder ved Institutt for prosess-, energi- og miljøteknologi",
      "SA_TITTELINTERN": null,
      "SA_ANSVFIRMANAVN": "Universitetet i Sørøst-Norge",
      "SA_ARKIVREF": "23/19056",
      "SA_ANSVEPOST": "kari.nordmann@usn.no",
      "JOURNALPOST_TAB": {
        "JOURNALPOST": {
          "JP_INNHOLD": "Postmelding - Information that position is being withdrawn - 23/117",
          "JP_U1": "UO",
          "JP_DOKDATO": "29.11.2023",
          "JP_DOKTYPE": "Dokument ut",
          "ADRESSAT_TAB": {
            "ADRESSAT": {
              "NAVN": "Til søkere",
              "ADRESSE": null,
              "POSTNR": null,
              "POSTSTED": null,
              "EPOSTADR": null,
              "TLF": null
            }
          },
          "DOKVERSJON_TAB": {
            "DOKVERSJON": [
              {
                "VE_NAVN": "Postmelding",
                "VE_DOKFORMAT": "PDF",
                "VE_FILREF": "20231129100116_251323_431272_3983107_CollectionMailDialog.pdf",
                "VE_DOKID": null
              }
            ]
          }
        }
      }
    }
  }
}
{
  "NOARK_H": {
    "NOARKSAK": {
      "STILLINGSID": "251323",
      "SA_TITTEL": "Instituttleder ved Institutt for prosess-, energi- og miljøteknologi",
      "SA_TITTELINTERN": null,
      "SA_ANSVFIRMANAVN": "Universitetet i Sørøst-Norge",
      "SA_ARKIVREF": "23/19056",
      "SA_ANSVEPOST": "kari.nordmann@usn.no",
      "JOURNALPOST_TAB": {
        "JOURNALPOST": {
          "JP_INNHOLD": "Utvidet søkerliste  - 23/117",
          "JP_U1": "UO",
          "JP_DOKDATO": "29.11.2023",
          "JP_DOKTYPE": "Internt notat/e-post uten oppfølging",
          "ADRESSAT_TAB": {
            "ADRESSAT": {
              "NAVN": null,
              "ADRESSE": null,
              "POSTNR": null,
              "POSTSTED": null,
              "EPOSTADR": null,
              "TLF": null
            }
          },
          "DOKVERSJON_TAB": {
            "DOKVERSJON": [
              {
                "VE_NAVN": "Utvidet søkerliste",
                "VE_DOKFORMAT": "PDF",
                "VE_FILREF": "20231129100109_251323_431271_3983105_ExtendedApplicantsList.pdf",
                "VE_DOKID": null
              }
            ]
          }
        }
      }
    }
  }
}
{
  "NOARK_H": {
    "NOARKSAK": {
      "STILLINGSID": "251323",
      "SA_TITTEL": "Instituttleder ved Institutt for prosess-, energi- og miljøteknologi",
      "SA_TITTELINTERN": null,
      "SA_ANSVFIRMANAVN": "Universitetet i Sørøst-Norge",
      "SA_ARKIVREF": "23/19056",
      "SA_ANSVEPOST": "kari.nordmann@usn.no",
      "JOURNALPOST_TAB": {
        "JOURNALPOST": {
          "JP_INNHOLD": "Offentlig søkerliste  - 23/117",
          "JP_U1": "U",
          "JP_DOKDATO": "29.11.2023",
          "JP_DOKTYPE": "Internt notat/e-post uten oppfølging",
          "ADRESSAT_TAB": {
            "ADRESSAT": {
              "NAVN": null,
              "ADRESSE": null,
              "POSTNR": null,
              "POSTSTED": null,
              "EPOSTADR": null,
              "TLF": null
            }
          },
          "DOKVERSJON_TAB": {
            "DOKVERSJON": [
              {
                "VE_NAVN": "Offentlig søkerliste",
                "VE_DOKFORMAT": "PDF",
                "VE_FILREF": "20231129100107_251323_431270_3983103_PublicApplicantsList.pdf",
                "VE_DOKID": null
              }
            ]
          }
        }
      }
    }
  }
}
```

* * *
