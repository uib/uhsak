# Utkast til retningslinjer for arkivering direkte i Documaster (UH sak)

## Tilgjengelige API og oppsett

### Noark 5 tjenestegrensesnitt

Dette er det foretrukne API-et, da det er en åpen standard og det i fremtidige anskaffelser kan kreves at det blir implementert.

Documasters implementasjon avviker på noen områder fra standarden. Dette vil bli dokumentert senere. Det har trolig ingen betydning for typiske operasjoner utført av arkivintegrasjoner.

Dokumentasjon: https://github.com/arkivverket/noark5-tjenestegrensesnitt-standard


### Documaster Noark 5 Web Services

Dette er Documasters proprietære API. Sikt har laget en adapter (i Java) for å bruke dette.

Dokumentasjon:

* https://documaster.github.io/api-docs/general/
* https://github.com/documaster/noark5-web-services

**NB:** Meld mangler i dokumentasjonen til per.gaustad@uib.no, som vil ta det videre i prosjektet.


### GeoIntegrasjon

Neppe aktuell for applikasjoner vi utvikler selv. Mer informasjon kommer.

### Autentisering og oppsett av API

En kan autentisere seg mot API-ene som et system (med RSA-sertifikat og JWT) eller en bruker (med Feide-pålogging).[^autentisering]

Det vanlige er å autentisere seg som et system. Da genereres JWT-et av Gravitee, hvor RSA-sertifikatet er lagret. Applikasjoner konsumerer API-ene via Gravitee.

Vi venter fortsatt på tilbakemelding fra leverandøren beskriver fremgangsmåten etter det første steget nedenfor. Men den er muligens omtrent slik:

1. Kunden oppretter et API i Gravitee, genererer et RSA-sertifikat og x5t (som beskrevet [her](https://git.app.uib.no/it-bott-integrasjoner/intark/-/blob/master/gw/doc/documaster.md).
1. Kunden sender den offentlige delen av sertifikatet samt x5t-verdien til leverandøren.
1. Leverandøren knytter sertifikatet til ekstern gruppe («external group», «claim») og bekrefter at x5t-verdien er som forventet.
1. Noen oppretter en [tilgangsgruppe](https://github.com/documaster/noark5-web-services/blob/develop/version1/06%20-%20Access%20control.md#access-group) i Documaster og knytter den til den eksterne gruppen.

[^autentisering]: Fra leverandøren:

    > OAuth2 flows or JWT bearer tokens are used to authenticate against the APIs.
    >
    > **Authentication Flows – Feide Authenticated End User**
    >
    > Documaster Archive has integration with Feide platform for validating end user issued access tokens from OAuth2 Authorization Code Grant.
    Quality system authenticating an end user against Feide, can use the acquired access token to access some of
    the provided APIs.
    >
    > * Feide access token for an end user can be provided and validated against Feide (using userinfo endpoint)
    > * End user access groups are retrieved from ServiceNow based on the sub ID of the user in Feide
    >
    > **Authentication Flows – Service Context**
    >
    > Documaster Archive exposed APIs can be accessed in service context, where the authentication method is not identifying an end user, but a system. Feide does not support service accounts, so authentication/authorization of service requests is done against Documaster archive.
    >
    > * Integrating system produces JWT signed with RSA certificate to be used as bearer token (in Authorization header) against the API
    > * Documaster configuration contains the certificate used to validate the provided JWT and the available access group to which the consuming system is granted access


## Tilgangsstyring for systembruker i Documaster

Kommer.


## Entiteter

### Arkiv (Opprettes i ServiceNow.)

Entiteten «Arkiv» tilsvarer funksjon i [FUP-en](https://fupperåd.no).

Eksempel på `arkiv.tittel`: «A Internadministrasjon»

Entiteten opprettes i ServiceNow, ikke av andre integrasjoner.


### Arkivdel (Opprettes i ServiceNow.)

Entiteten «Arkivdel» tilsvarer underfunksjon i [FUP-en](https://fupperåd.no).

Eksempel på `arkivdel.tittel`: «A Internadministrasjon»: «A.d Rekruttering og ansettelse av personale»

Entiteten opprettes i ServiceNow, ikke av andre integrasjoner.


### Primært klassifikasjonssystem (Opprettes i ServiceNow.)

Entiteten (primært) «Klassifikasjonssystem» tilsvarer underfunksjon i [FUP-en](https://fupperåd.no).

Eksempel på `klassifikasjonssystem.tittel`: «A.d Rekruttering og ansettelse av personale»

Entiteten opprettes i ServiceNow, ikke av andre integrasjoner.


### Primær klasse (klasse fra primært klassifikasjonssystem)

Entiteten «Klasse» i primært klassifikasjonssystem tilsvarer prosess i [FUP-en](https://fupperåd.no).

Eksempel på `klasse.tittel`: «A.d.01 Rekruttere»

Entiteten opprettes i ServiceNow, ikke av andre integrasjoner.


### Sekundære klassifikasjonssystem og klasser

Det kan finnes flere sekundære klassifikasjonssystem enn de som er nevnt nedenfor, for den enkelte prosessen, for eksempel for rekrutteringstype eller prosjektnummer. De bør være like ved alle institusjoner.

Mer informasjon kommer.

Sekundære klassifikasjonssystemer opprettes i ServiceNow, ikke av andre integrasjoner.


#### OU (administrativ enhet)

* `klassifikasjonssystem.tittel`: «OU»

* `klasse.klasseIdent`: stien i til enheten Orgreg, for eksempel «1.760.761» for enheten «Rektor og styre»
* `klasse.tittel`: navnet til enheten i Orgreg, for eksempel «Rektor og styre»


#### ROLF (tilgangsstyring) (Skal inntil videre ikke opprettes av arkivintegrasjoner)

* `klassifikasjonssystem.tittel`: «ROLF»

Hierarki av klasser hvor nederste nivå har `klasse.id` = `funksjon` + `:` + `sti_i_orgreg`, for eksempel «A.J.05:1.760.764.1200».

Det gjenstår å avklare med leverandøren hvordan disse skal brukes av arkivintegrasjoner.

Se <https://www.uio.no/tjenester/it/adm-app/rolf/> for mer informasjon om ROLF.

### Saksmappe

* `administrativEnhet`: Verdi uavklart, se DFCT0012187 i Agile. Vil være en funksjon av objektet i Orgreg. Se <https://documaster.github.io/api-docs/operations/other/codelists/> for å finne eventuelle eksisterende verdiet i miljøet.

    Hvis administrativEnhet skal være den samme som administrativ enhetenhet i kildesystem. Oversett verdi fra kildesystem til uoId i Orgreg og bruk denne.

* `skjerming`: hjemmel for unntak fra innsyn. Verdien hentes fra kodelisten «skjerming»: for eksempel «offl. § 21»


### Journalpost

* `saksbehandler`: fritekst, navnet på den som har behandlet registreringen i kildesystemet
* `skjerming`: hjemmel for unntak fra innsyn. Verdien hentes fra kodelisten «skjerming»: for eksempel «offl. § 21»
* `skjermKorrespondanseParterEInnsyn`: Boolsk verdi som angir om korrespondanseparter skal skjermes i offentlig journal eller ikke.


### Tilgangskode og tilgangsgrupper

Styres av det sekundære klassifikasjonssystemet «ROLF» (se ovenfor).

Det finnes ikke tilgangskoder i Documaster.. En bruker mappe.skjerming og registrering.skjerming for hjemmel for å unnta dokumentet fra innsyn. Verdien hentes fra kodelisten «skjerming»: for eskempel «offl. § 21»


## Utkast til mappingdokument ved bestilling av integrasjoner.

Kommer.


## Integrasjoner

Planlagt eller under utvikling. Spørsmålstegn hvis status er ukjent.

* toa-ms (arkivering av kontrakter fra toa-løsningen (SAP))
* Integrasjoner levert av Sikt: https://unit.atlassian.net/wiki/spaces/IPM/pages/2380005377/UH-SAK
    - ansettelsessaker fra Jobbnorge
    - saker som gjelder klage over karakterfastsetting
    - pensumlister fra Leganto
    - eksamensoppgaver
    - sensorveiledninger?
    - masteroppgaver
    - programplaner/emneplaner? («stoppet i påvente av FS-API-er»)
    - fra Flyt (saksbehandling i FS)
* kontrakter fra TendSign
<!--
* permisjonssaker fra SAP?
* oppsigelsessaker fra SAP?
-->
