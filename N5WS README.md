---
title: N5WS README
date: 24.07.2023
...

# Introduction

Documaster officially supports v1.0 of the Noark 5 Web Services specification unless otherwise specified in the Deviations section below.

This document lists the intricacies and deviations of the Documaster Noark 5 Web Services implementation as compared to that version.

Deviations from v1.0 of the specification

The deviations are described in three categories:

* *Additions*\
    Backwards-compatible support for functionality not required in v1.0

* *Supported differently*\
    Functionality implemented differently than specified in v1.0.

* *Not supported*\
    Functionality required by v1.0, but not implemented in Documaster.

# Additions

This category contains functionality that is not required by v1.0 of the specification, but Documaster has added support for it either because it is supported in v1.1 or because Documaster has found it useful to do so while preserving backwards-compatibility.

## Code list and Business-specific metadata creation

The creation endpoints for code lists and Business-specific metadata are a v1.1 feature of the specification, but Documaster has addded support for them. The following additional ny- endpoints are supported:

* https://rel.arkivverket.no/noark5/v5/api/metadata/ny-arkivdelstatus/
* https://rel.arkivverket.no/noark5/v5/api/metadata/ny-arkivstatus/
* https://rel.arkivverket.no/noark5/v5/api/metadata/ny-dokumentmedium/
* https://rel.arkivverket.no/noark5/v5/api/metadata/ny-dokumentstatus/
* https://rel.arkivverket.no/noark5/v5/api/metadata/ny-dokumenttype/
* https://rel.arkivverket.no/noark5/v5/api/metadata/ny-graderingskode/
* https://rel.arkivverket.no/noark5/v5/api/metadata/ny-kassasjonsvedtak/
* https://rel.arkivverket.no/noark5/v5/api/metadata/ny-klassifikasjonstype/
* https://rel.arkivverket.no/noark5/v5/api/metadata/ny-koordinatsystem/
* https://rel.arkivverket.no/noark5/v5/api/metadata/ny-merknadstype/
* https://rel.arkivverket.no/noark5/v5/api/metadata/ny-skjermingdokument/
* https://rel.arkivverket.no/noark5/v5/api/metadata/ny-skjermingmetadata/
* https://rel.arkivverket.no/noark5/v5/api/metadata/ny-slettingstype/
* https://rel.arkivverket.no/noark5/v5/api/metadata/ny-tilknyttetregistreringsom/
* https://rel.arkivverket.no/noark5/v5/api/metadata/ny-variantformat/
* https://rel.arkivverket.no/noark5/v5/api/metadata/ny-avskrivningsmaate/
* https://rel.arkivverket.no/noark5/v5/api/metadata/ny-elektronisksignatursikkerhetsnivaa/
* https://rel.arkivverket.no/noark5/v5/api/metadata/ny-elektronisksignaturverifisert/
* https://rel.arkivverket.no/noark5/v5/api/metadata/ny-flytstatus/
* https://rel.arkivverket.no/noark5/v5/api/metadata/ny-journalposttype/
* https://rel.arkivverket.no/noark5/v5/api/metadata/ny-journalstatus/
* https://rel.arkivverket.no/noark5/v5/api/metadata/ny-korrespondanseparttype/
* https://rel.arkivverket.no/noark5/v5/api/metadata/ny-land/
* https://rel.arkivverket.no/noark5/v5/api/metadata/ny-postnummer/
* https://rel.arkivverket.no/noark5/v5/api/metadata/ny-presedensstatus/
* https://rel.arkivverket.no/noark5/v5/api/metadata/ny-partrolle/
* https://rel.arkivverket.no/noark5/v5/api/metadata/ny-sakspartrolle/
* https://rel.arkivverket.no/noark5/v5/api/metadata/ny-saksstatus/
* https://rel.arkivverket.no/noark5/v5/api/metadata/ny-tilgangskategori/
* https://rel.arkivverket.no/noark5/v5/api/metadata/ny-virksomhetsspesifikkeMetadata/

## OData functions, operators, and lambdas

The specification requires support for OData, but it does not state what conformity level should be achieved.

Regardless, none of the conformity levels require much more than the equality operator in OData filters.

Documaster supports the following OData functions and operators:

* *string*: contains, startswith, endswith
* *logical*: eq, ne, gt, ge, lt, le, and, or
* *lambda*: any

# Supported differently

This category contains functionality that is described in the specification, but Documaster has deemed necessary to add support for it slightly differenly, mostly to preserve compliance with Noark 5.5.

## Nested entities required during creation

To be 100% compliant with the standard, Documaster requires that:

* Arkiv creation requests (ny-arkiv) contain at least one nested arkivskaper
* Journalpost creation requests (ny-journalpost) contain at least one nested korrespondansepart

This is because neither of these entities are allowed to exist without a child by the Noark 5.5 standard (arkivskaper or korrespondansepart).

## Upload

Documaster supports both small and large file uploads as defined in v1.0 of the specification. However, the specification clashes with the requirements of the Noark 5 standard, because the specification requires that a dokumentobjekt is created prior to uploading and linking a physical document to it whereas the Noark 5 standard requires that a dokumentobjekt has a linked physical document at all times.

To achieve compliance with the standard, Documaster has modified the specification\'s requirements as little as possible, but a change is present nonetheless.

In short, Documaster reuses the same fields and endpoints, but:

1.  Changes the sequence of operations \
    Documaster requires a document to be uploaded prior to creating a document object
2.  Requires the "referanseDokumentfil" field to be specified upon creating the document object.

A Documaster upload request would look like this:

1.  Upload document (where the content-disposition header is optional, but recommended so that the file can be uploaded with a specific filename)

    ```
    POST .../fil
    Content-disposition: VALUE

    binary stream

    ---

    {
      "_links": {
        "self": {
          "href": "https://FQDN/n5ws/v1/struktur/fil/2485693/"
        }
      }
    }
    ```

    The response contains a link that must be used when subsequently creating the dokumentobjekt

2.  Create dokumentobjekt and link the uploaded document as part of the creation

    ```
    POST .../ny-dokumentobject
    {
      "format": "pdf",
      "variantformat": {
        "kode": "P"
      },
      "referanseDokumentfil": "https://FQDN/n5ws/v1/struktur/fil/2485693/"
    }
    ```

The same approach has been adopted by large file uploads where the large file upload returns the same response as step 1 described above.

## Change and access logs

Documaster has implemented support for the eventlogs as described in v1.1 of the specification. Note, v1.1, not v1.0, because v1.0 lists incorrect hendelsetype code list values.

Documaster has imposed some limitations on the allowed OData filters, however, to be able to support the specification\'s requirements while at the same time not having to rewrite its archival core.

Change log requests (loggingogsporing/endringslogg) have the following limitations:

* \$search, \$orderby, \$expand query parameters are not supported
* \$filter query parameter is limited to the following expressions: `referanseArkivenhet eq 'ID'`

Event log requests (loggingogsporing/hendelseslogg) have the following limitations:

* \$search, \$orderby, \$expand query parameters are not supported
* \$filter query parameter is limited to one of the following two expressions: `referanseArkivenhet eq 'ID' and hendelsetype/kode eq 'R'` (implying an access log request) or `referanseArkivenhet eq 'ID' and hendelsetype/kode ne 'R'` (implying a changelog request)

Additionally, Documaster supports the following access log entities:

* mappe
* saksmappe
* basisregistrering
* journalpost
* arkivnotat
* dokumentbeskrivelse

and the following change log entities and fields:

* Arkiv#(tittel,arkivstatus)
* Arkivskaper#(arkivskaperNavn)
* Arkivdel#(tittel,arkivdelstatus,skjerming,klassifikasjonssystem)
* Klassifikasjonssystem#(tittel,klassifikasjonstype)
* Klasse#(klasseID,tittel,skjerming,kassasjon)
* Mappe#(tittel,offentligTittel,kassasjon,skjerming,klasse,arkivdel)
* Saksmappe#(tittel,offentligTittel,kassasjon,skjerming,saksstatus,referanseAdministrativEnhet,saksdato,saksansvarlig,kl asse,arkivdel)
* Basisregistrering#(tittel,offentligTittel,forfatter,kassasjon,skjerming,mappe/saksmappe) Journalpost#
* (tittel,offentligTittel,forfatter,kassasjon,skjerming,journaldato,dokumentetsDato,mottattDato,sendtDato, offentlighetsvurdertDato,journalstatus,journalposttype,saksmappe)
* Arkivnotat#(tittel,offentligTittel,forfatter,kassasjon,skjerming,dokumentetsDato,mottattDato,sendtDato,offentlighets vurdertDato,saksmappe)
* Dokument#(tittel,kassasjon,skjerming,dokumentstatus,registrering)
* Part#(navn)
* Korrespondansepart#(navn,korrespondanseparttype,referanseAdministrativEnhet,registrering)

## Hendelsetype list

Endpoint https://rel.arkivverket.no/noark5/v5/api/metadata/ny-hendelsetype/ is not supported by

Documaster and although accessible, it will return an error if an attempt to use it is made. In other words, the hendelsetype code list is hard-coded to a predefined set of values.

The hendelsetype codes are taken from version 1.1 of the specification and are:

* C (Opprettet)
* R (Lest)
* U (Endret)
* D (Slettet)

as opposed to the less meaningful Endringslogg, Søknad mottatt, Søknad komplett, Vedtak which seem like an honest error in version 1.0.

## User ID SystemID fields

Documaster reads the user ID fields from the connected external identity provider for populating data such access log, change log, and certain user ID fields such as referanseAvsluttetAv, referanseOpprettetAv, referanseArkivertAv, among others. Said identity provider could, for example, be Azure, Feide, etc., and Documaster is entirely dependent on the format of the user identifier in it. This makes Documaster as compliant with the requirement that these fields are SystemIDs as the corresponding identity provider. Documaster cannot and should not obfuscate the user IDs by re-generating them as UUIDs, because that would be performed via an (inherently irreversible) hash function which means Documaster and/or integrations would have to lose the user reference which, in turn, would be a security-related issue when backtracing user actions upon a potential security event.

# Not supported

This category contains functionality that is described in the specification, but Documaster does not support it due to security or functional concerns.

## Missing Tilgang object

Documaster's access control is much more complex and versatile than what v1.0 of the specification requires making it impossible for Documaster to meet the requirements without risking loss of access control configuration or outright misdirections of clients that use it. Since this is a security-related risk, Documaster has not implemented support for the Tilgang entity.

## Missing Bruker object

Documaster has not implemented support for the Bruker object which is a non-standard object and which is not necessarily expected to exist by an archival business system since users usually reside in an external user directory (AD, Azure, Google, etc.).

## Missing KorrespondanseIntern fields

Field KorrespondanseIntern#referanseSaksbehandler is not supported by Documaster as this optional field does not exist in the Noark 5 standard.
